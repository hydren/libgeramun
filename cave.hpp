/*
 * cave.hpp
 *
 *  Created on: 22/06/2014
 *      Author: felipe
 */

#ifndef LIBGERAMUN_CAVE_HPP_
#define LIBGERAMUN_CAVE_HPP_

#include "util.hpp"

void cave(Grid& grid, int flag, unsigned row, unsigned col, unsigned len, float probMud=0.2, int brushSize=1, float brushSizeVar=0, int iniDir=-1);


#endif /* LIBGERAMUN_CAVE_HPP_ */
