/*
 * surf_replacer.cpp
 *
 *  Created on: 21/05/2014
 *      Author: felipe
 */

#include "surf_replacer.hpp"

#include "preview_callback.hpp"

/** Replace all surface blocks (using target_flag as reference) with 'new_flag' */
void surface_replace(Grid& grid, int target_flag, int new_flag)
{
	for(unsigned j = 0; j < grid[0].size(); j++)
	{
		unsigned surface = 0;
		while(grid[surface][j] != target_flag and surface < grid.size())
			surface++;

		if(surface < grid.size() and surface > 0 //segurança
			and grid[surface-1][j] == 0) //verificação importante
		{
			grid[surface][j] = new_flag;

			call_preview_grid_callback(grid);
		}
	}
}
