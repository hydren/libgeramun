/*
 * miners.hpp
 *
 *  Created on: Apr 27, 2014
 *      Author: faguiar
 */

//Reference:  http://noelberry.ca/2011/04/procedural-generation-the-caves/

#ifndef LIBGERAMUN_MINERS_HPP_
#define LIBGERAMUN_MINERS_HPP_

#include "util.hpp"

//FixMe change the probability variable type from int to float/double
void miners_algorithm(Grid& grid, int flag, GridRegion area, unsigned numberOfMiners, int spawnProbability);
void miners_algorithm2(Grid& grid, int flag, int bg_flag, unsigned numberOfMiners, int spawnProbability);


#endif /* LIBGERAMUN_MINERS_HPP_ */
