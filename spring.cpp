/*
 * spring.cpp
 *
 *  Created on: Apr 27, 2014
 *      Author: faguiar
 */

#include "spring.hpp"

#include "preview_callback.hpp"

#include <iostream>
#include <cstdio>
#include <algorithm>

using std::cout;
using std::endl;
using std::vector;

unsigned SPRING_MAX_ITERATIONS = 500000;

// global debug flag
extern bool LIBGERAMUN_DEBUG;

struct drop
{
	unsigned col, row;
	bool active;
	char cameFrom;
	unsigned nofall_count;

	//to speed up things
	drop(unsigned row, unsigned col)
	: col(col), row(row), active(true), cameFrom(NORTH), nofall_count(0)
	{}
};

//ToDo use the flood fill algorithm to detect and remove isolate drops (1 or 2 blocks)

void spring(Grid& map, int flag, unsigned volume, unsigned row, unsigned col)
{
	unsigned mapWidth = map[0].size();

	vector<drop> drops;
	unsigned inactives = 0;
	unsigned iteration_counter = 1;

	drop c (row, col);
	drops.push_back(c);

	if(LIBGERAMUN_DEBUG) cout << "running..." << endl;

	while( inactives != volume and iteration_counter < SPRING_MAX_ITERATIONS ) {
		++iteration_counter;

		call_preview_grid_callback(map);

		//moving all drops
		for(unsigned int i=0; i < drops.size(); i++)
		{
			//check free left
			bool esq_livre;
			if(drops[i].col == 0 or map[drops[i].row][drops[i].col-1] != 0)
				esq_livre = false;
			else
				esq_livre = true;

			//check free right
			bool dir_livre;
			if(drops[i].col == map[0].size()-1 or map[drops[i].row][drops[i].col+1] != 0)
				dir_livre = false;
			else
				dir_livre = true;

			//check free down
			bool baixo_livre;
			if(drops[i].row == map.size()-1 or map[drops[i].row+1][drops[i].col] != 0)
				baixo_livre = false;
			else
				baixo_livre = true;


			if(baixo_livre) //downwards free
			{
				drops[i].nofall_count = 0;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].row += 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = NORTH;
			}
			else if(drops[i].nofall_count > 2*mapWidth)
			{
				drops[i].active = false;
			}
			else if(esq_livre and not dir_livre)  //only left free
			{
				drops[i].nofall_count++;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col -= 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = EAST;
			}
			else if(dir_livre and not esq_livre)  //only right free
			{
				drops[i].nofall_count++;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col += 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = WEST;
			}
			else if(esq_livre and dir_livre) //both left and right free
			{
				drops[i].nofall_count++;
				drops[i].active = true;

				int tmp = 0;

				//keep direction
				if(drops[i].cameFrom == EAST)
					tmp = -1;

				//keep direction
				else if(drops[i].cameFrom == WEST)
					tmp = +1;

				else //coming from up, settle on luck
				{
					//settle direction randomly
					while(tmp == 0) {
						tmp = (rand()%3) - 1;
					}

//					//settle direction based on index
//					if (drops[i].row % 2 == 0)
//						tmp = 1;
//					else
//						tmp = -1;
				}

				//move drop
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col += tmp;
				map[drops[i].row][drops[i].col] = flag;
			}
			else //all directions blocked
			{
				drops[i].active = false;
			}
		}

		//creating drops
		if( drops.size() < volume) {
			drop _c(row, col);
			drops.push_back(_c);
		}

		//count inactives
		inactives = 0;
		for(unsigned int i=0; i < drops.size(); i++) {
			if(not drops[i].active)
				inactives++;
		}
	}
	if(LIBGERAMUN_DEBUG) 	cout << "spring iterations: " << iteration_counter << endl;
}

void springn(Grid& map, int flag, unsigned volume, vector<GridRegion> v, float drop_prob)
{
	const unsigned mapWidth = map[0].size();

	vector<drop> drops;
	unsigned inactives = 0;
	unsigned iteration_counter = 1;

	vector<GridRegion> springs(v);
	std::random_shuffle(springs.begin(), springs.end());

	//gush first droplets from each spring
	for(unsigned i=0; i<springs.size(); i++)
	{
		if(randomBetween(0, 100) < drop_prob*100)
		drops.push_back(drop(springs[i].y, springs[i].x));
	}

	if(LIBGERAMUN_DEBUG) 	cout << "running..." << endl;

	while( inactives != volume and iteration_counter < SPRING_MAX_ITERATIONS ) {
		++iteration_counter;

		//code to create many files from each step
//		if(iteration_counter%100 == 0)
//		{
//			snapshot(map, string("snapshots/snap")
//					+(iteration_counter/10000)+" "
//					+(iteration_counter%10000)/1000+" "
//					+(((iteration_counter%10000)%1000)/100)+" "
//					+((((iteration_counter%10000)%1000)%100)/10)+" "
//					+((((iteration_counter%10000)%1000)%100)%10)
//			+".bmp");
//			cout << "Proceed?" << endl;
//			getchar();
//		}

		call_preview_grid_callback(map);

		std::random_shuffle(drops.begin(), drops.end());

		//moving all drops
		for(unsigned int i=0; i < drops.size(); i++)
		{
			//check left free
			bool esq_livre;
			if(drops[i].col == 0 or map[drops[i].row][drops[i].col-1] != 0)
				esq_livre = false;
			else
				esq_livre = true;

			//check right free
			bool dir_livre;
			if(drops[i].col == map[0].size()-1 or map[drops[i].row][drops[i].col+1] != 0)
				dir_livre = false;
			else
				dir_livre = true;

			//checar downwards free
			bool baixo_livre;
			if(drops[i].row == map.size()-1 or map[drops[i].row+1][drops[i].col] != 0)
				baixo_livre = false;
			else
				baixo_livre = true;


			if(baixo_livre) //downwards free
			{
				drops[i].nofall_count = 0;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].row += 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = NORTH;
			}
			else if(drops[i].nofall_count > 2*mapWidth)
			{
				drops[i].active = false;
			}
			else if(esq_livre and not dir_livre)  //only left free
			{
				drops[i].nofall_count++;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col -= 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = EAST;
			}
			else if(dir_livre and not esq_livre)  //only right free
			{
				drops[i].nofall_count++;
				drops[i].active = true;
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col += 1;
				map[drops[i].row][drops[i].col] = flag;
				drops[i].cameFrom = WEST;
			}
			else if(esq_livre and dir_livre) //both left and right free
			{
				drops[i].nofall_count++;
				drops[i].active = true;

				int tmp = 0;

				//keep direction
				if(drops[i].cameFrom == EAST)
					tmp = -1;

				//keep direction
				else if(drops[i].cameFrom == WEST)
					tmp = +1;

				else //coming from up, settle on luck
				{
					//settle direction randomly
					while(tmp == 0) {
						tmp = (rand()%3) - 1;
					}

					//settle direction based on index
//					if (drops[i].row % 2 == 0)
//						tmp = 1;
//					else
//						tmp = -1;
				}

				//move the droplet
				map[drops[i].row][drops[i].col] = 0;
				drops[i].col += tmp;
				map[drops[i].row][drops[i].col] = flag;
			}
			else //all directions blocked
			{
				drops[i].active = false;
			}
		}

		//creating drops from all sources
		if( drops.size() < volume) {
			std::random_shuffle(springs.begin(), springs.end());
			for(unsigned i=0; i<springs.size() and drops.size() < volume ; i++)
			{
				if(randomBetween(0, 100) < drop_prob*100)
				drops.push_back(drop(springs[i].y, springs[i].x));
			}
		}

		//count inactives
		inactives = 0;
		for(unsigned int i=0; i < drops.size(); i++) {
			if(not drops[i].active)
				inactives++;
		}

	}
	if(LIBGERAMUN_DEBUG) cout << "springn iterations: " << iteration_counter << endl;
}
