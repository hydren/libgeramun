/*
 * generators.cpp
 *
 *  Created on: 01/10/2013
 *      Author: carlosfaruolo
 */

#include "generators.hpp"

#include "util.hpp"
#include <iostream>
#include <cstdlib>
#include <cmath>
#include "cave.hpp"
#include "midpoint_displacement.hpp"
#include "miners.hpp"
#include "random_walk.hpp"
#include "spring.hpp"
#include "surf_replacer.hpp"

using std::cout;
using std::endl;
using std::vector;

//Generators

/** Fills (portion*100) % of the grid with 1 */
Grid generator1(unsigned width, unsigned height, float portion)
{
	Grid grid = createGrid(width, height);
	fillPartially(grid, 1, portion);
	return grid;
}

/** Uses the midpoint displacement algorithm. The 'factor' parameter controls the displacement smoothness.
 * 	This version starts with a half filled grid.
 * */
Grid generator2a(unsigned width, unsigned height, float factor)
{
	Grid grid = generator1(width, height);
	midpoint_displacement(grid, 1, factor);
	return grid;
}

/**	Uses the midpoint displacement algorithm. The 'factor' parameter controls the displacement smoothness.
 * 	This version starts with a empty grid, choosing the borders' heights randomly.
 *  */
Grid generator2b(unsigned width, unsigned height, float factor)
{
	Grid grid = createGrid(width, height);

	unsigned left_height = randomBetween(0, grid.size()-1);
	unsigned right_height = randomBetween(0, grid.size()-1);
	for(unsigned i = 0; i < grid.size(); i++)
	{
		if(i < left_height)
			grid[i][0] = 0;
		else
			grid[i][0] = 1;

		if(i < right_height)
			grid[i][grid[i].size()-1] = 0;
		else
			grid[i][grid[i].size()-1] = 1;
	}

	midpoint_displacement(grid, 1, factor);
	return grid;
}

//XXX Needs to change to a implementation which keeps the aspect ratio, adapting the factor
Grid generator2c(unsigned width, unsigned height, float factor)
{
	return generator2b(width, height, factor);
}

/** Generate a map using a random walk algorithm. Applies the algorithm in a half filled grid. */
Grid generator3a(unsigned width, unsigned height, float change_prob, unsigned distance, unsigned brushSize)
{
	Grid grid = generator1(width, height, 0.5);
	random_walk_4dir(grid, 0, randomBetween(0, height-1), randomBetween(0, width-1), distance, change_prob, brushSize);
	return grid;
}

//ToDo Make more variations of generators using random walk.

/** Generator using miners algorithm (random walk variation) */
Grid generator4a(unsigned width, unsigned height, float change_prob, unsigned numberOfMiners)
{
	Grid g = generator1(width, height, 0.5);
	miners_algorithm(g, 0, GridRegion(width*0.1, height*0.1, width*0.8, height*0.8), numberOfMiners, 100*change_prob);
	return g;
}

Grid generator2a3a(unsigned width, unsigned height, float factor, float change_prob, unsigned distance, unsigned brushSize)
{
	Grid g = generator2a(width, height, factor);
	random_walk_4dir(g, 0, randomBetween(0, height-1), randomBetween(0, width-1), distance, change_prob, brushSize);
	return g;
}

Grid generator2b3a(unsigned width, unsigned height, float factor, float change_prob, unsigned distance, unsigned brushSize)
{
	Grid g = generator2b(width, height, factor);
	random_walk_4dir(g, 0, randomBetween(0, height-1), randomBetween(0, width-1), distance, change_prob, brushSize);
	return g;
}

Grid& generator5(Grid& grid)
{
	cout << "executing using recommended parameters" << endl;

	unsigned height_real = grid.size();
	unsigned width_real = grid[0].size();

	//map borders at 50% height
//	fillPartially(grid, 1, 0.5);

	//random height map borders
	{
//		unsigned left_height = randomBetween(0, grid.size()-1);
//		unsigned right_height = randomBetween(0, grid.size()-1);

		unsigned left_height = (9*grid.size())/10;
		unsigned right_height = (9*grid.size())/10;

		fillGridColumn(grid, 1, 0				 , left_height, true);
		fillGridColumn(grid, 1, grid[0].size()-1, right_height , true);
	}

	midpoint_displacement(grid, 1, 0.8, 0.70);

	//ToDo use mdisp on 90% of the grid and use mdisp on borders (0-5%) going from the bottom to the middle, simulating a beach

//	midpoint_displacement(grid, 2, 0.99, 0.70);

	unsigned map_area = width_real * height_real;

	unsigned num_miners = (unsigned)(((double)map_area)*0.0008);
	double probSpawn = 131072/((double) map_area);
	cout << "executing miners algorithm to spawn rocks (x" << num_miners << ", p=" << probSpawn << ")..." << endl;
//	miners_algorithm(grid, 2, rect(width_real/8, (6*height_real)/8, (6*width_real)/8, height_real/8), 100, 0.5);
	miners_algorithm2(grid, 2, 1, num_miners, probSpawn); //200

//	random_walk_8dir(grid, 0, height_real/2, width_real/3,     2000, 0.1, 2);
//	random_walk_8dir(grid, 0, height_real/2, (2*width_real)/3, 2000, 0.1, 2);

	int num_rocks = (int)(((double)width_real)*0.016); //16
	cout << "executing 4-direction random walk algorithm to spawn rocks (x"<< num_rocks << ")..." << endl;
	for(int i = 2; i < num_rocks+2; i++)
	{
//		random_walk_4dir(grid, 2, (randomBetween(6,9)*height_real)/10, (i*width_real)/(num_rocks+3), 200, 0.75, 1);

		unsigned k = 0;
		while(grid[k][(i*width_real)/(num_rocks+3)] == 0) k++;
		if(k == height_real-1) continue;
		k = (k + height_real-1)/2;
		random_walk_4dir(grid, 2, k, (i*width_real)/(num_rocks+3), 200, 0.75, 1);
	}

	int num_caves = int(((double)map_area)*0.000031); //8
	cout << "executing random walk algorithm to make caves (x"<< num_caves << ")..."  << endl;
	for(int i = 2; i < num_caves+2; i++)
	{
		unsigned k = 0;
		while(grid[k][(i*width_real)/(num_caves+3)] == 0) k++;
		if(k ==height_real-1) continue;
		k += randomBetween(0, (height_real-1-k));
		cave(grid, 0, k, (i*width_real)/(num_caves+3), 500, 0.25, 3, 0.5);
	}

	unsigned volume = 2*width_real; //2048
	cout << "executing cellular automata algorithm (water rule) to generate oceanic borders (volume="<< volume << "x2)..." << endl;

	spring(grid, 3, volume, 0, 1);
	spring(grid, 3, volume, 0, width_real-1);

	volume = width_real/2; //512
	unsigned n_springs = width_real/4;
	cout << "executing cellular automata algorithm (water rule) to create small lakes (rain points="<< n_springs << ", volume=" << volume <<")..." << endl;
	float spring_offset = ((float)width_real/(float)(n_springs+1));
	vector<GridRegion> fontes;
	for(unsigned x = 1; x < n_springs+1; x++) {
		if(randomBetween(0, 3))
			fontes.push_back(GridRegion(x*spring_offset+randomBetween(-spring_offset, spring_offset), 0));
	}
	springn(grid, 3, volume, fontes, 0.5);

	cout << "generating grass..." << endl;
	surface_replace(grid, 1, 4);

	return grid;
}


