/*
 * util.hpp
 *
 *  Created on: 3 de out de 2017
 *      Author: carlosfaruolo
 */

#ifndef LIBGERAMUN_UTIL_HPP_
#define LIBGERAMUN_UTIL_HPP_
#include <ciso646>

#include <vector>

#include <cstdlib>
#include <cmath>

// Cardinal points
#define NORTH 0
#define EAST 1  // right
#define WEST 2  // left
#define SOUTH 3

// Extra cardinal points
#define SOUTHEAST 4
#define SOUTHWEST 5
#define NORTHEAST 6
#define NORTHWEST 7

/** Returns a random integer between 'min' and 'max' (in the range [min, max]) */
inline int randomBetween(int min, int max)
{
	int x;
	if (max == min)
		return min;

	if(max < min) //swap
	{
		const int tmp = max;
		max = min;
		min = tmp;
	}

	do x = rand();
	while ((max-min) < RAND_MAX && x >= RAND_MAX - (RAND_MAX % (max-min)));
	return min + x % (max-min);
}

// To ease writing
typedef std::vector< std::vector<int> > Grid;

/** Create a grid with the specified size, initialized with the specified value
 * columns		Number of columns of the resulting matrix, or the width of the map
 * lines		Number of lines of the reuslting matrix, or the height of the map
 * */
inline Grid createGrid(unsigned columns, unsigned lines, int value=0)
{
	return std::vector< std::vector<int> >(lines, std::vector<int>(columns, value));
}

/** Fills partially the given grid with the specified flag, from bottom up, until it reached the specified fraction */
inline void fillPartially(Grid& grid, int flag, float fraction)
{
	if(fraction > 1 or fraction < 0) return; //tem que ser entre 0 e 1
	unsigned bound = grid.size() * (1 - fraction);
	for(unsigned i = grid.size() - 1; i >= bound; i--)
		for(unsigned j = 0; j < grid[i].size(); j++)
			grid[i][j] = flag;
}

/** Fills the column of the specified with the specifiend flag up to the given height.*/
inline void fillGridColumn(Grid& grid, int flag, unsigned column, unsigned height, bool cleanOthers=false)
{
	if(cleanOthers) for(unsigned i = 0; i < grid.size(); i++)
	{
		if(i < height)
			grid[i][column] = 0;
		else
			grid[i][column] = flag;
	}

	else for(unsigned i = height; i < grid.size(); i++)
			grid[i][column] = flag;
}

struct GridRegion
{
	int x, y;
	unsigned width, height;
	GridRegion(int x1, int y1, unsigned w=0, unsigned h=0) : x(x1), y(y1), width(w), height(h) {}
};

#endif /* LIBGERAMUN_UTIL_HPP_ */
