/*
 * spring.hpp
 *
 *  Created on: Apr 27, 2014
 *      Author: faguiar
 */

#ifndef LIBGERAMUN_SPRING_HPP_
#define LIBGERAMUN_SPRING_HPP_

//Reference    http://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/

#include "util.hpp"

void spring(Grid& map, int flag, unsigned volume, unsigned row, unsigned col);

void springn(Grid& map, int flag, unsigned volume, std::vector<GridRegion> spring_list, float drop_prob=1);


#endif /* LIBGERAMUN_SPRING_HPP_ */
