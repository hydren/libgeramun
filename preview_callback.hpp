/*
 * preview_callback.hpp
 *
 *  Created on: 4 de out de 2017
 *      Author: carlosfaruolo
 */

#ifndef LIBGERAMUN_PREVIEW_CALLBACK_HPP_
#define LIBGERAMUN_PREVIEW_CALLBACK_HPP_
#include <ciso646>

#include "util.hpp"

extern void (*preview_grid_callback)(const Grid& grid);

inline void call_preview_grid_callback(const Grid& grid)
{
	if(preview_grid_callback != NULL)
		preview_grid_callback(grid);
}

#endif /* LIBGERAMUN_PREVIEW_CALLBACK_HPP_ */
