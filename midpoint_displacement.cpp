/*
 * midpoint_displacement.cpp
 *
 *  Created on: 06/04/2014
 *      Author: felipe
 */

#include "midpoint_displacement.hpp"

#include "preview_callback.hpp"

#include <string>
#include <iostream>
#include <math.h>

using std::string;
using std::cout;
using std::endl;

// global debug flag
extern bool LIBGERAMUN_DEBUG;

//To keep data without pass by argument
Grid grid;
int flag;
float factor;

string depth; //for debugging

//prototypes
void recursive_midpoint_displacement(unsigned index_i, unsigned index_f, float range=1);


/**
 * Applies the midpoint displacement algorithm on the grid, with the specified flag.
 * The factor specify the smoothness.
 * If debug is true, shows debug texts.
 * */
void midpoint_displacement(Grid& g, int _flag, float _factor, float range)
{
	if(factor < 0) // the factor needs to be more than 0, preferable between 0 and 1
		return;

	//setting parameters
	grid = g;
	flag = _flag;
	factor = _factor;

	if(LIBGERAMUN_DEBUG)
		depth = ""; //debug

	//calling recursive method
	recursive_midpoint_displacement(0, grid[0].size() - 1, range);
	g = grid; //getting the result
}


void recursive_midpoint_displacement(unsigned index_i, unsigned index_f, float range)
{
	if(LIBGERAMUN_DEBUG)
	{
		depth = depth+"=";
		cout << depth << "> entering the interval (" << index_i << ", " << index_f << "), with range = " << range;
	}

	if(index_f - index_i < 2) //when it is not possible to have more midpoints
	{
		if(LIBGERAMUN_DEBUG)
		{
			cout << " ~~~* index_f - index_i = " << (index_f - index_i) << " < 2. Stop." << endl;
			depth = depth.substr(1);
		}
		return;
	}
	else if(LIBGERAMUN_DEBUG)
		cout << endl;

	//compute the midpoint
	unsigned midpoint = (index_i + index_f)/2;

	//compute the borders' heights
	unsigned index_i_height=0, index_f_height=0;

	while(index_i_height < grid.size() and grid[index_i_height][index_i]!=flag)
		index_i_height++;

	while(index_f_height < grid.size() and grid[index_f_height][index_f]!=flag)
		index_f_height++;

	//set the midpoint height as the mean of the range borders' heights
	unsigned midpoint_height = (index_i_height + index_f_height)/2;

	//compute the displacement. Cannot be outside range. Varies with the range.
	int displacement = range * randomBetween(-midpoint_height, grid.size()-midpoint_height);

	//displace the midpoint height
	midpoint_height += displacement;

	//fills according to the new height
	fillGridColumn(grid, flag, midpoint, midpoint_height);

	call_preview_grid_callback(grid);

	//shortens the range (the range starts with 1 and decays along the calls, according to the factor value
	range *= pow(2, -factor);

	//calls this method to the new intervals
	recursive_midpoint_displacement(index_i, midpoint, range);
	recursive_midpoint_displacement(midpoint, index_f, range);

	if(LIBGERAMUN_DEBUG)
		depth = depth.substr(1);
}
