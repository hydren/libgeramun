/*
 * brush.cpp
 *
 *  Created on: 07/04/2014
 *      Author: felipe
 */

#include "brush.hpp"

#include <iostream>

using std::cout;
using std::endl;

/**
 * Applies a brush with the flag's value, on the specified line and column.
 * */
void circle_brush(Grid& grid, int flag, unsigned brushSize, unsigned row, unsigned col)
{
	unsigned mapWidth = grid[0].size(), mapHeight = grid.size();

	int x,y,radiusError;
	x = brushSize;
	y = 0;
	radiusError = 1-x;

	while(x >= y)
	{
		for(unsigned i=col-x; i<=col+x; i++)
			if( i < mapWidth )
				for(unsigned j=row-y; j<=row+y; j++)
					if( j < mapHeight)
						grid[j][i] = flag;

		for(unsigned i=col-y; i<=col+y; i++)
			if(i < mapWidth)
				for(unsigned j=row-x; j<=row+x; j++)
					if(j < mapHeight)
						if( j < mapHeight)
							grid[j][i] = flag;

		y++;
		if(radiusError<0)
			radiusError+=2*y+1;
		else
		{
			x--;
			radiusError+=2*(y-x+1);
		}
	}

}

//FixMe still bugged
void diamond_brush(Grid& grid, int flag, unsigned brushSize, unsigned row, unsigned col)
{
	for( unsigned i=0; i < brushSize; i++ ) {
		for( unsigned j=0; j < brushSize-i; j++ ) {
			grid[col+i][row+j] = flag;
		}
	}
	for( unsigned i=0; i < brushSize; i++ ) {
		for( unsigned j=0; j < brushSize-i; j++ ) {
			grid[col-i][row+j] = flag;
		}
	}
	for( unsigned i=0; i < brushSize; i++ ) {
		for( unsigned j=0; j < brushSize-i; j++ ) {
			grid[col+i][row-j] = flag;
		}
	}
	for( unsigned i=0; i < brushSize; i++ ) {
		for( unsigned j=0; j < brushSize-i; j++ ) {
			grid[col-i][row-j] = flag;
		}
	}

}
