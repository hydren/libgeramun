/*
 * random_walk.hpp
 *
 *  Created on: 07/04/2014
 *      Author: felipe
 */

#ifndef LIBGERAMUN_RANDOM_WALK_HPP_
#define LIBGERAMUN_RANDOM_WALK_HPP_

#include "brush.hpp"
#include "util.hpp"

//Reference:  http://pcg.wikidot.com/pcg-algorithm:drunkard-walk

void random_walk_4dir(Grid& grid, int flag, unsigned row=0, unsigned col=0, unsigned length=50, float switchProbability=0.5, int brushsize=4);

void random_walk_8dir(Grid& grid, int flag, unsigned row=0, unsigned col=0, unsigned length=50, float switchProbability=0.5, int brushsize=4);


#endif /* LIBGERAMUN_RANDOM_WALK_HPP_ */
