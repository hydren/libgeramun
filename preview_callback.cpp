/*
 * preview_callback.cpp
 *
 *  Created on: 4 de out de 2017
 *      Author: carlosfaruolo
 */

#include "preview_callback.hpp"

typedef void (*preview_grid_callback_type)(const Grid&);
preview_grid_callback_type preview_grid_callback = NULL;
