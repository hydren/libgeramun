/*
 * midpoint_displacement.hpp
 *
 *  Created on: 06/04/2014
 *      Author: felipe
 */

#ifndef LIBGERAMUN_MIDPOINT_DISPLACEMENT_HPP_
#define LIBGERAMUN_MIDPOINT_DISPLACEMENT_HPP_

#include "util.hpp"

//Reference:  http://gameprogrammer.com/fractal.html#midpoint

/**
 * Applies the midpoint displacement algorithm on the grid, with the specified flag.
 * The factor specify the smoothness.
 * If debug is true, shows debug texts.
 * */
void midpoint_displacement(Grid& grid, int flag, float fator, float range=1);


#endif /* LIBGERAMUN_MIDPOINT_DISPLACEMENT_HPP_ */
