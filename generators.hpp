/*
 * generators.hpp
 *
 *  Created on: 01/10/2013
 *      Author: carlosfaruolo
 */

#ifndef LIBGERAMUN_GENERATORS_HPP_
#define LIBGERAMUN_GENERATORS_HPP_

#include "util.hpp"

//simple generators
Grid generator1(unsigned largura, unsigned altura, float fracao=0.5);

//midpoint displacement generators
Grid generator2a(unsigned largura, unsigned altura, float fator=0.5);
Grid generator2b(unsigned largura, unsigned altura, float fator=0.5);
Grid generator2c(unsigned largura, unsigned altura, float fator=0.5);

//random walk generators
Grid generator3a(unsigned largura, unsigned altura, float prob_mud=0.5, unsigned distancia=25, unsigned brushSize=2);

//miner's algorithm generators
Grid generator4a(unsigned largura, unsigned altura, float prob_mud=0.1, unsigned numeroDeMineradores=100);

//midpoint displacement with random walk generators
Grid generator2a3a(unsigned largura, unsigned altura, float fator=0.5, float prob_mud=0.5, unsigned distancia=25, unsigned brushSize=2);
Grid generator2b3a(unsigned largura, unsigned altura, float fator=0.5, float prob_mud=0.5, unsigned distancia=25, unsigned brushSize=2);

Grid& generator5(Grid& grid);

#endif /* LIBGERAMUN_GENERATORS_HPP_ */
