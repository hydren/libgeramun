/*
 * cave.cpp
 *
 *  Created on: 22/06/2014
 *      Author: felipe
 */

#include "cave.hpp"

#include "brush.hpp"
#include "preview_callback.hpp"

#include <iostream>

using std::cout;
using std::endl;

//de main.cpp
extern bool LIBGERAMUN_DEBUG;

void cave(Grid& grid, int flag, unsigned row, unsigned col, unsigned lenght, float changeProb, int ini_brush_size, float brushSizeVar, int iniDir)
{
	unsigned mapaLargura = grid[0].size(), mapaAltura = grid.size();
	unsigned brushSize = ini_brush_size;
	int maxbrushsize = ((float)ini_brush_size)*(1+brushSizeVar);
	int minbrushsize = ((float)ini_brush_size)*(1-brushSizeVar);

	if(minbrushsize < 0) minbrushsize = 0;

	if(LIBGERAMUN_DEBUG)
		cout << "aplicando cave com coordenadas iniciais: (" << row << ", " << col << ") - [brushsize range: " << minbrushsize << "/" << maxbrushsize << "]" << endl;

	unsigned contador = 0;
	int delta_i, delta_j;

	short dir;
	if(iniDir == -1)
		dir = randomBetween(NORTH, NORTHWEST+1);
	else
		dir = iniDir;

	short new_dir; //auxiliar

	while(contador < lenght)
	{
		call_preview_grid_callback(grid);
		if(LIBGERAMUN_DEBUG)
			cout << "direcao: ";
		switch(dir)
		{
			case NORTH:
				delta_i = -1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "norte" << endl;
				break;
			case EAST:
				delta_i =  0;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "leste" << endl;
				break;
			case WEST:
				delta_i =  0;
				delta_j = -1;
				if(LIBGERAMUN_DEBUG)
					cout << "oeste" << endl;
				break;
			case SOUTH:
				delta_i =  1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "sul" << endl;
				break;
			case SOUTHEAST:
				delta_i =  1;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "sudeste" << endl;
				break;
			case SOUTHWEST:
				delta_i =  1;
				delta_j =  -1;
				if(LIBGERAMUN_DEBUG)
					cout << "sudoeste" << endl;
				break;
			case NORTHEAST:
				delta_i =  -1;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "nordeste" << endl;
				break;
			case NORTHWEST:
				delta_i =  -1;
				delta_j =  -1;
				if(LIBGERAMUN_DEBUG)
					cout << "noroeste" << endl;
				break;

			default: //nao deve ocorrer
				delta_i = 0;
				delta_j = 0;
				if(LIBGERAMUN_DEBUG)
					cout << "???" << endl;
				break;
		}

		row += delta_i;
		col += delta_j;

		if(LIBGERAMUN_DEBUG)
			cout << "  tentando posicão: (" << row << ", " << col << ")" << endl;

		//protecao de indice
		if( (row < mapaAltura)  and  (col < mapaLargura) )
		{
			if(LIBGERAMUN_DEBUG)
				cout << "   aplicando <" << flag << "> em (" << row << ", " << col << ")" << endl;
			circle_brush(grid, flag, brushSize, row, col);
			contador++;
		}
		else
		{
			if(LIBGERAMUN_DEBUG) cout << "Random walk fora dos limites! Parar." << endl;
			break;
		}

		//chance de mudar de direcao
		if(randomBetween(0, 100) < changeProb*100)
		{
			int x = randomBetween(0, 100);
			int low = 5, mid=25;
			switch(dir)
			{
				case NORTH:
					if(x > mid) new_dir = randomBetween(0, 2) ? NORTHEAST : NORTHWEST;
					else if(x > low) new_dir = randomBetween(0, 2) ? EAST : WEST;
					else new_dir = randomBetween(0, 2) ? SOUTHEAST : SOUTHWEST;
					break;
				case EAST:
					if(x > mid) new_dir = randomBetween(0, 2) ? NORTHEAST : SOUTHEAST;
					else if(x > low) new_dir = randomBetween(0, 2) ? NORTH : SOUTH;
					else new_dir = randomBetween(0, 2) ? NORTHWEST : SOUTHWEST;
					break;
				case WEST:
					if(x > mid) new_dir = randomBetween(0, 2) ? NORTHWEST : SOUTHWEST;
					else if(x > low) new_dir = randomBetween(0, 2) ? NORTH : SOUTH;
					else new_dir = randomBetween(0, 2) ? NORTHEAST : SOUTHEAST;
					break;
				case SOUTH:
					if(x > mid) new_dir = randomBetween(0, 2) ? SOUTHWEST : SOUTHEAST;
					else if(x > low) new_dir = randomBetween(0, 2) ? EAST : WEST;
					else new_dir = randomBetween(0, 2) ? NORTHWEST : NORTHEAST;
					break;
				case SOUTHEAST:
					if(x > mid) new_dir = randomBetween(0, 2) ? EAST : SOUTH;
					else if(x > low) new_dir = randomBetween(0, 2) ? NORTHEAST : SOUTHWEST;
					else new_dir = randomBetween(0, 2) ? WEST : NORTH;
					break;
				case SOUTHWEST:
					if(x > mid) new_dir = randomBetween(0, 2) ? WEST : SOUTH;
					else if(x > low) new_dir = randomBetween(0, 2) ? NORTHWEST : SOUTHEAST;
					else new_dir = randomBetween(0, 2) ? EAST : NORTH;
					break;
				case NORTHEAST:
					if(x > mid) new_dir = randomBetween(0, 2) ? EAST : NORTH;
					else if(x > low) new_dir = randomBetween(0, 2) ? SOUTHEAST : NORTHWEST;
					else new_dir = randomBetween(0, 2) ? WEST : SOUTH;
					break;
				case NORTHWEST:
					if(x > mid) new_dir = randomBetween(0, 2) ? WEST : NORTH;
					else if(x > low) new_dir = randomBetween(0, 2) ? SOUTHWEST : NORTHEAST;
					else new_dir = randomBetween(0, 2) ? EAST : SOUTH;
					break;

				default: //shouldn't happen
					do new_dir = randomBetween(NORTH, NORTHWEST+1);
					while(new_dir == dir);
					break;
			}
			dir = new_dir;
			do	{ x = brushSize + randomBetween(-1, 2);
			}while(x > maxbrushsize or x < minbrushsize);

			brushSize = x;
		}
	}
}
