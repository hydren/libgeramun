/*
 * miners.cpp
 *
 *  Created on: Apr 27, 2014
 *      Author: faguiar
 */

#include "miners.hpp"
#include "preview_callback.hpp"

using std::vector;

// global debug flag

struct miner
{
	unsigned col, row;
	bool active;

	miner() : col(0), row(0), active(false) {}
	miner(unsigned col, unsigned row, bool active=true ) : col(col), row(row), active(active) {}
};

/**
 * Algorithm suitable to creating caves.
 * Creates many 'miners' on the specified region, filling the blocks with the specified flag.
 *
 * @param Grid map
 * The map grid (matrix of ints)
 *
 * @param int flag
 * Flag to fill the cave
 *
 * @param Rect area
 * Region to work on. Anything outside the region is untouched by the algorithm.
 *
 * @param unsigned numberOfMiners
 * Number of miners to spawn (stop criteria)
 *
 * @param int spawnProbability
 * A number between 0 and 100.
 * The probability of creating a new miner each iteration. The smaller this value is, the more conected and long will be the caves.
 */
void miners_algorithm(Grid& map, int flag, GridRegion area, unsigned numberOfMiners, int spawnProbability)
{
	unsigned mapWidth = map[0].size(), mapHeight = map.size();

	miner m;
	m.col = (area.x + mapWidth)/2; //starts from the center
	m.row = (area.y + mapHeight)/2; //starts from the center
	m.active = true;

	vector<miner> miners;
	unsigned int dir;
	vector<int> validMoves;

	miners.push_back(m);

	while (miners.size() < numberOfMiners ) //stop criteria
	{
		for (unsigned int i = 0; i < miners.size(); ++i)
		{
			if ( miners[i].active )
			{
//				cout << i << "-> got in.." << endl;

				//figure out which movements are valid to the miner
				if ( miners[i].row > 0           and map[ miners[i].row-1 ][ miners[i].col   ] != flag ) validMoves.push_back(NORTH);
				if ( miners[i].col+1 < mapWidth  and map[ miners[i].row   ][ miners[i].col+1 ] != flag ) validMoves.push_back(WEST);
				if ( miners[i].row+1 < mapHeight and map[ miners[i].row+1 ][ miners[i].col   ] != flag ) validMoves.push_back(SOUTH);
				if ( miners[i].col > 0           and map[ miners[i].row   ][ miners[i].col-1 ] != flag ) validMoves.push_back(EAST);

				if ( validMoves.size() > 0 )
				{
					map[ miners[i].row ][ miners[i].col ] = flag; //fills the current block with the flag
					dir =  validMoves[ randomBetween(0, validMoves.size()) ]; //picks a direction randomly
					validMoves.clear();

					if ( dir == NORTH ) { //north
//						cout << i << "-> walked north!" << endl;
						miners[i].row--;
					}
					else if ( dir == WEST ) { //west
//						cout << i << "-> walked west!" << endl;
						miners[i].col++;
					}
					else if ( dir == SOUTH ) { //south
//						cout << i << "-> walked south!" << endl;
						miners[i].row++;
					}
					else if ( dir == EAST ) { //east
//						cout << i << "-> walked east!" << endl;
						miners[i].col--;
					}

					if ( map[ miners[i].row ][ miners[i].col ] != flag ) {
						map[ miners[i].row ][ miners[i].col ] = flag;
						if ( rand() % 100 + 1 <= spawnProbability  && miners.size() < numberOfMiners ) { //the smaller the probability, the more conected are the blocks.
							miners.push_back( miner(randomBetween(area.x, area.x + area.width),randomBetween(area.y, area.y + area.height)) );
						}
					}
				}
				else
				{
					//if reaches here, its because no IF was triggered
					//so its a isolated point
					miners[i].active = false;
					if ( miners.size() < numberOfMiners )
						miners.push_back( miner(randomBetween(area.x, area.x + area.width),randomBetween(area.y, area.y + area.height)) );

				}
				call_preview_grid_callback(map);
			}
		}

		unsigned int count = 0;
		for (unsigned int i = 0; i < miners.size(); ++i) {
			if (!miners[i].active){
				count ++;
//				cout << i << "-> deactivated" << endl;
			}
		}
		if(count == numberOfMiners) break;
	}
}



/**
 * Algorithm suitable to creating caves.
 * Creates many 'miners' on the grid, filling the blocks with the specified flag, when the blocks are flagged with the former specified flag.
 *
 * @param Grid map
 * The map grid (matrix of ints)
 *
 * @param int flag
 * Flag to fill the cave
 *
 * @param int bg_flag
 * Flag corresponding to the block type considered valid to modify. Hence, the blocks with flags other than bg_flag are ignored.
 *
 * @param unsigned numberOfMiners
 * Number of miners to spawn (stop criteria)
 *
 * @param int spawnProbability
 * A number between 0 and 100.
 * The probability of creating a new miner each iteration. The smaller this value is, the more conected and long will be the caves.
 */
void miners_algorithm2(Grid& map, int flag, int bg_flag, unsigned numberOfMiners, int spawnProbability)
{
	unsigned mapWidth = map[0].size(), mapHeight = map.size();

	miner m;
	m.col = mapWidth/2; //starts from the center
	m.row = mapHeight/2; //starts from the center
	m.active = true;

	vector<miner> miners;
	unsigned int dir;
	vector<int> validMoves;

	miners.push_back(m);

	while (miners.size() < numberOfMiners ) //stop criteria
	{
		for (unsigned int i = 0; i < miners.size(); ++i)
		{
			if ( miners[i].active )
			{
//				cout << i << "-> got in.." << endl;

				//figure out which movements are valid to the miner
				if ( miners[i].row > 0           and map[ miners[i].row-1 ][ miners[i].col   ] == bg_flag ) validMoves.push_back(NORTH);
				if ( miners[i].col+1 < mapWidth  and map[ miners[i].row   ][ miners[i].col+1 ] == bg_flag ) validMoves.push_back(WEST);
				if ( miners[i].row+1 < mapHeight and map[ miners[i].row+1 ][ miners[i].col   ] == bg_flag ) validMoves.push_back(SOUTH);
				if ( miners[i].col > 0           and map[ miners[i].row   ][ miners[i].col-1 ] == bg_flag ) validMoves.push_back(EAST);

				if ( validMoves.size() > 0 )
				{
					map[ miners[i].row ][ miners[i].col ] = flag; //preenche o bloco onde está com a flag
					dir =  validMoves[ randomBetween(0, validMoves.size()) ]; //escolhe um sentido aleatoriamente
					validMoves.clear();

					if ( dir == NORTH ) { //north
//						cout << i << "-> walked north!" << endl;
						miners[i].row--;
					}
					else if ( dir == WEST ) { //west
//						cout << i << "-> walked west!" << endl;
						miners[i].col++;
					}
					else if ( dir == SOUTH ) { //south
//						cout << i << "-> walked south!" << endl;
						miners[i].row++;
					}
					else if ( dir == EAST ) { //east
//						cout << i << "-> walked east!" << endl;
						miners[i].col--;
					}

					if ( map[ miners[i].row ][ miners[i].col ] != flag ) {
						map[ miners[i].row ][ miners[i].col ] = flag;
						if ( rand() % 100 + 1 <= spawnProbability  && miners.size() < numberOfMiners ) { //the smaller the probability, the more conected are the blocks.
							int i = randomBetween(0, mapHeight);
							int j = randomBetween(0, mapWidth);
							while(map[i][j]!=bg_flag)
							{
								i = randomBetween(0, mapHeight);
								j = randomBetween(0, mapWidth);
							}
							miners.push_back( miner(j, i) );
						}
					}
				}
				else
				{
					//if reaches here, its because no IF was triggered
					//so its a isolated point
					miners[i].active = false;
					if ( miners.size() < numberOfMiners )
					{
						//miners.push_back( miner(randomBetween(area.x, area.x + area.width),randomBetween(area.y, area.y + area.height)) );
						int i = randomBetween(0, mapHeight);
						int j = randomBetween(0, mapWidth);
						while(map[i][j]!=bg_flag)
						{
							i = randomBetween(0, mapHeight);
							j = randomBetween(0, mapWidth);
						}
						miners.push_back( miner(j, i) );
					}

				}
				call_preview_grid_callback(map);
			}
		}

		unsigned int count = 0;
		for (unsigned int i = 0; i < miners.size(); ++i) {
			if (!miners[i].active){
				count ++;
//				cout << i << "-> deactivated" << endl;
			}
		}
		if(count == numberOfMiners) break;
	}
}
