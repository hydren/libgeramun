/*
 * brush.hpp
 *
 *  Created on: 07/04/2014
 *      Author: felipe
 */

#ifndef LIBGERAMUN_BRUSH_HPP_
#define LIBGERAMUN_BRUSH_HPP_

#include "util.hpp"

enum BrushType
{
	CIRCLE, DIAMOND
};

/**
 * Applies a brush with the flag's value, on the specified line and column.
 * */
void circle_brush(Grid& grid, int flag, unsigned brushSize, unsigned row, unsigned col);

// ToDo description for diamond_brush
void diamond_brush(Grid& grid, int flag, unsigned brushSize, unsigned row, unsigned col);



#endif /* LIBGERAMUN_BRUSH_HPP_ */
