# libgeramun
Stochastic, procedural platformer-game map generation routines.

The routines are meant to produce maps with a Terraria-like look and feel.

This library is used in the tool [geramundo](https://github.com/hydren/geramundo).
