/*
 * surf_replacer.hpp
 *
 *  Created on: 21/05/2014
 *      Author: felipe
 */

#ifndef LIBGERAMUN_SURF_REPLACER_HPP_
#define LIBGERAMUN_SURF_REPLACER_HPP_

#include "util.hpp"

/** Replace all surface blocks (using target_flag as reference) with 'new_flag' */
void surface_replace(Grid& grid, int target_flag, int new_flag);


#endif /* LIBGERAMUN_SURF_REPLACER_HPP_ */
