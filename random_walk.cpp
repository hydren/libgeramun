/*
 * random_walk.cpp
 *
 *  Created on: 07/04/2014
 *      Author: felipe
 */

#include "random_walk.hpp"

#include "preview_callback.hpp"

#include <iostream>

using std::cout;
using std::endl;

// global debug flag
extern bool LIBGERAMUN_DEBUG;

void random_walk_4dir(Grid& grid, int flag, unsigned row, unsigned col, unsigned length, float switchProbability, int brushSize)
{
	unsigned mapaLargura = grid[0].size(), mapaAltura = grid.size();

	if(LIBGERAMUN_DEBUG)
		cout << "applying random_walk_4dir with initial coordinates: (" << row << ", " << col << ")" << endl;

	unsigned contador = 0;
	int delta_i, delta_j;

	short dir = randomBetween(NORTH, SOUTH+1);
	short new_dir; //auxiliar

	while(contador < length)
	{
		call_preview_grid_callback(grid);

		if(LIBGERAMUN_DEBUG)
			cout << "direction: ";
		switch(dir)
		{
			case NORTH:
				delta_i = -1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "north" << endl;
				break;
			case EAST:
				delta_i =  0;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "east" << endl;
				break;
			case WEST:
				delta_i =  0;
				delta_j = -1;
				if(LIBGERAMUN_DEBUG)
					cout << "west" << endl;
				break;
			case SOUTH:
				delta_i =  1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "south" << endl;
				break;
			default: //nao deve ocorrer
				delta_i = 0;
				delta_j = 0;
				if(LIBGERAMUN_DEBUG)
					cout << "???" << endl;
				break;
		}

		row += delta_i;
		col += delta_j;

		if(LIBGERAMUN_DEBUG)
			cout << "  trying position: (" << row << ", " << col << ")" << endl;

		//protecao de indice
		if( (row < mapaAltura)  and  (col < mapaLargura) )
		{
			if(LIBGERAMUN_DEBUG)
				cout << "   appying <" << flag << "> at (" << row << ", " << col << ")" << endl;
			circle_brush(grid, flag, brushSize, row, col);
			contador++;
		}
		else
		{
			if(LIBGERAMUN_DEBUG) cout << "random_walk_4dir outside limits! Stop." << endl;
			break;
		}

		//chance de mudar de direcao
		if(randomBetween(0, 100) < switchProbability*100)
		{
			do new_dir = randomBetween(NORTH, SOUTH+1);
			while(new_dir == dir);

			dir = new_dir;
		}
	}
}

void random_walk_8dir(Grid& grid, int flag, unsigned row, unsigned col, unsigned length, float switchProbability, int brushSize)
{
	unsigned mapaLargura = grid[0].size(), mapaAltura = grid.size();

	if(LIBGERAMUN_DEBUG)
		cout << "applying random_walk_8dir with initial coordinates: (" << row << ", " << col << ")" << endl;

	unsigned contador = 0;
	int delta_i, delta_j;

	short dir = randomBetween(NORTH, NORTHWEST+1);
	short new_dir; //auxiliary

	while(contador < length)
	{
		call_preview_grid_callback(grid);

		if(LIBGERAMUN_DEBUG)
			cout << "direction: ";
		switch(dir)
		{
			case NORTH:
				delta_i = -1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "north" << endl;
				break;
			case EAST:
				delta_i =  0;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "east" << endl;
				break;
			case WEST:
				delta_i =  0;
				delta_j = -1;
				if(LIBGERAMUN_DEBUG)
					cout << "west" << endl;
				break;
			case SOUTH:
				delta_i =  1;
				delta_j =  0;
				if(LIBGERAMUN_DEBUG)
					cout << "south" << endl;
				break;
			case SOUTHEAST:
				delta_i =  1;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "southeast" << endl;
				break;
			case SOUTHWEST:
				delta_i =  1;
				delta_j =  -1;
				if(LIBGERAMUN_DEBUG)
					cout << "southwest" << endl;
				break;
			case NORTHEAST:
				delta_i =  -1;
				delta_j =  1;
				if(LIBGERAMUN_DEBUG)
					cout << "northeast" << endl;
				break;
			case NORTHWEST:
				delta_i =  -1;
				delta_j =  -1;
				if(LIBGERAMUN_DEBUG)
					cout << "northwest" << endl;
				break;

			default: //nao deve ocorrer
				delta_i = 0;
				delta_j = 0;
				if(LIBGERAMUN_DEBUG)
					cout << "???" << endl;
				break;
		}

		row += delta_i;
		col += delta_j;

		if(LIBGERAMUN_DEBUG)
			cout << "  trying position: (" << row << ", " << col << ")" << endl;

		//index protection
		if( (row < mapaAltura)  and  (col < mapaLargura) )
		{
			if(LIBGERAMUN_DEBUG)
				cout << "   applying <" << flag << "> at (" << row << ", " << col << ")" << endl;
			circle_brush(grid, flag, brushSize, row, col);
			contador++;
		}
		else
		{
			if(LIBGERAMUN_DEBUG) cout << "random_walk_8dir outside limits! Stop." << endl;
			break;
		}

		//direction change probability
		if(randomBetween(0, 100) < switchProbability*100)
		{
			do new_dir = randomBetween(NORTH, NORTHWEST+1);
			while(new_dir == dir);

			dir = new_dir;
		}
	}
}
